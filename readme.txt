=== Pxl ===
Contributors: Luiz Alberto
Tags: pxl
Requires at least: 4.7
Tested up to: 5.4
Stable tag: 4.3
Requires PHP: 7.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A Toolbelt for Wordpress development

https://gitlab.com/api/v4/projects/madeinnordeste%2Fpxl-sandbox-wordpress-plugin/repository/archive.zip?sha=v0.0.1

== Description ==

A Toolbelt for Wordpress development :)

= Packages =

* [Blade](https://github.com/duncan3dc/blade/)

* [Carbon Fields](https://carbonfields.net/)

* [php-form](https://github.com/rlanvin/php-form)


== FAQ ==

= What is the meaning of life, universe and everything else? =

42

== Changelog ==

= 0.0.1 =

* Start

