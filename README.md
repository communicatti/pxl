# Pxl

A Toolbelt for Wordpress development

## Namespace

    \Pxl

---

## Helpers

### Blade

Use a Blade template engine

#### instance()

Get a Blade instance

    $blade = \Pxl\Blade::instance($views_path = null, $cache_path = null);

- if **\$views_path** is null, the current dir is defined

- if **\$cache_path** is null, the fold with AUTH_KEY name in sys_get_temp_dir() is defined

### Post

#### image()

Return a post featured image URL if is set or \$defaultURL

    $image = \Pxl\Post::image($post_id, $default = null);

#### meta()

Return a post meta value(s)

    $meta = \Pxl\Post::meta($post_id, $key, bool $single = true);

#### permalink_by_slug()

Return a permalink by slug

     $permalink = \Pxl\Post::permalink_by_slug($slug);

#### taxonomy_name()

Return a taxonomy name

    $taxonomy_name = \Pxl\Post::taxonomy_name($post_id, $taxonomy);

#### pagination()

Return a posts pagination

    $pagination = \Pxl\Post::pagination($args = [], $per_page);

- **\$args** see get_posts - https://developer.wordpress.org/reference/functions/get_posts/

#### get_posts_keys()

Get a list with posts keys (ID, slug, permalink, title)

    $posts_keys = get_posts_keys($args = []);

- **\$args** see get_posts - https://developer.wordpress.org/reference/functions/get_posts/

### Recaptcha

#### get_posts_keys()

Validate token, return a boolean

    $valid = \Pxl\Recaptcha::validate($token);

#### scripts()

Print reCaptcha scripts

    \Pxl\Recaptcha::scripts($css_class = '.recaptcha_token')

- **\$css_class** class to recaptcha script populate after get token

### WhatsApp

#### get_api_url()

Return a custom or default WhatsApp API URL

    $api_url = \Pxl\WhatsApp::get_api_url($message, $phone);


---

## How Publish a new version

To release version 1.2.3, create a new Git tag named `v1.2.3` or `1.2.3`. That's it.
[PUC](https://github.com/YahnisElsts/plugin-update-checker) doesn't require strict adherence to [SemVer](http://semver.org/). These are all valid tag names: `v1.2.3`, `v1.2-foo`, `1.2.3_rc1-ABC`, `1.2.3.4.5`. However, be warned that it's not smart enough to filter out alpha/beta/RC versions. If that's a problem, you might want to use GitHub releases or branches instead.

#### Example publish

change version in pxl.php file

    * Version:     0.0.4

Create and push a git tag

    git tag -a v0.0.4 -m 'v0.0.4'

    git push origin v0.0.4

## How to install Composer packages without dev dependencies

    composer install --no-dev

### Packages

- [Carbon Fields](https://carbonfields.net/)

* [Blade](https://github.com/duncan3dc/blade/)

* [php-form](https://github.com/rlanvin/php-form)

## Plugins

- [Carbon Fields](https://carbonfields.net/release-archive/)

- [Advanced Custom Fields](https://www.advancedcustomfields.com/)

- [Custom Post Type UI](https://github.com/WebDevStudios/custom-post-type-ui/)

- [Adminimize](https://wordpress.org/plugins/adminimize/)

- [MainWP Child](https://mainwp.com/)
