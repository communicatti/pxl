<?php

/**
 * Plugin Name: Pxl
 * Plugin URI:  https://gitlab.com/communicatti/pxl
 * Text Domain: pxl
 * Description: A Toolbelt for Wordpress development
 * Author:      Communicatti
 * Author URI:  http://communicatti.com
 * Version:     0.0.35
 *
 *
 * @package pxl
 * @author  Communicatti  <dev@communicatti.com>
 * @version 2021-03-03
 */

// CONSTANTS
define('PXL_PLUGIN_DIR', __DIR__);

// AUTOLOAD
require_once __DIR__ . '/vendor/autoload.php';

// PAGES
require __DIR__ . '/pages/admin.php';

// HELPERS
require __DIR__ . '/helpers/Post.php';
require __DIR__ . '/helpers/Recaptcha.php';
require __DIR__ . '/helpers/Blade.php';
require __DIR__ . '/helpers/WhatsApp.php';

// CONFIGS
require __DIR__ . '/carbon-fields.php';

// UPDATE CHECK
require_once __DIR__ . '/plugin-update-checker/plugin-update-checker.php';




function pxl_plugin_activation_actions()
{
	do_action('pxl_plugin_extension_activation');
}
register_activation_hook(__FILE__, 'pxl_plugin_activation_actions');

// Set default values
function pxl_plugin_default_options()
{
	$custom = pxl_get_custom_options();
	update_option('pxl_custom_core', $custom);
}
add_action('pxl_plugin_extension_activation', 'pxl_plugin_default_options');


function pxl_plugin_deactivate()
{
	delete_option('pxl_custom_core');
	//flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'pxl_plugin_deactivate');



function pxl_plugin_checker_setting()
{

	if (!is_admin() || !class_exists('Puc_v4_Factory')) {
		return;
	}

	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/communicatti/pxl',
		__FILE__,
		'pxl'
	);

	// (opt) If you're using a private repository, specify the access token like this:
	//$myUpdateChecker->setAuthentication('your-token-here');

	// (opt) Set the branch that contains the stable release.
	//$myUpdateChecker->setBranch('stable-branch-name');
}

add_action('admin_init', 'pxl_plugin_checker_setting');
